import { createApp } from 'vue'
import App from './App.vue'
import axios from 'axios'
import Plotly from 'plotly.js-dist'



import './assets/main.css'
let app = createApp(App);
app.config.globalProperties.axios=axios
app.config.globalProperties.plotly=Plotly
app.mount('#app')




