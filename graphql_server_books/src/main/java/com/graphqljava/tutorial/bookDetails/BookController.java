package com.graphqljava.tutorial.bookDetails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


@Controller
public class BookController {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @QueryMapping
    public Book bookById(@Argument int id) {
        String query = "SELECT * FROM BOOK WHERE ID = ?";
        List<Book>  listBook = jdbcTemplate.query(query, BeanPropertyRowMapper.newInstance(Book.class), id);
        if (listBook.size() > 0) {
            return  listBook.get(0);
        }
        return null;
    }

    @QueryMapping
    public List<Book> allBooks() {
        return jdbcTemplate.query("SELECT * FROM BOOK", new BeanPropertyRowMapper<Book>(Book.class));
    }

    @QueryMapping
    public List<Author> allAuthors() {
        System.out.println("-------allAuthors ----");
        return jdbcTemplate.query("SELECT * FROM AUTHOR", new BeanPropertyRowMapper<Author>(Author.class));
    }


    @SchemaMapping
    public Author author(Book book) {
        String query = "SELECT * FROM BOOK WHERE ID = ?";
        List<Author>  listAuthor = jdbcTemplate.query(query, BeanPropertyRowMapper.newInstance(Author.class),book.getAuthor());
        if (listAuthor.size() > 0) {
            return  listAuthor.get(0);
        }
        return null;
    }
}

