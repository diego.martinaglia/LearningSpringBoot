import { InMemoryCache, ApolloClient } from "@apollo/client";
import { createApolloProvider } from "@vue/apollo-option";

const cache = new InMemoryCache();

const apolloClient = new ApolloClient({
  cache,
  uri: "http://localhost:8080/graphql",
  fetchOptions: {
    mode: 'cors',
  },
});

export const provider = createApolloProvider({
  defaultClient: apolloClient,
});
