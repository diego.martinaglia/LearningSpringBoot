package ch.diego.demo1;

import ch.diego.demo1.dbservice.DBCloneService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@SpringBootTest
@PropertySource({
        "classpath:persistence-luz.properties"
})
@ComponentScan({"ch.diego.demo1.dbservice", "ch.diego.demo1.tasks"})
class Demo1ApplicationTests_2 {

    @Autowired
    DBCloneService dbCloneService;

    @Test
    void contextLoads() {
    }


    public String testInsertMthod() {
        dbCloneService.copyFromDB1_to_DB2();
        return "Hello";
    }

    @Test
    public void copyDataIntoH1() {
        // log.info("Start loading DAta {}", dateFormat.format(new Date()));
        dbCloneService.copyFromDB1_to_DB2();
    }


}
