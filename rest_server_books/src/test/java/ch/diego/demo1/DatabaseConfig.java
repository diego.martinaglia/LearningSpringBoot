package ch.diego.demo1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;


import javax.sql.DataSource;

@Configuration
public class DatabaseConfig {

    @Bean(name = "dbBooks")
    @ConfigurationProperties(prefix="spring.datasource.books")
    public DataSource bookDataSource() {
        DataSourceBuilder<?> ds= DataSourceBuilder.create();
        return ds.build();
    }

    @Bean(name = "jdbcSourceDB")
    @Autowired
    public NamedParameterJdbcTemplate bookJdbcTemplate(@Qualifier("dbBooks") DataSource dsSlave) {
        return new NamedParameterJdbcTemplate(dsSlave);
    }

    @Bean(name = "dbTmp")
    @ConfigurationProperties(prefix="spring.datasource.tmpdb")
    public DataSource tmpDBDataSource() {
        DataSourceBuilder<?> ds= DataSourceBuilder.create();
        return ds.build();
    }

    @Bean(name = "jdbcDestinationDB")
    @Autowired
    public NamedParameterJdbcTemplate tmpDBJdbcTemplate(@Qualifier("dbTmp") DataSource dsSlave) {
        return new NamedParameterJdbcTemplate(dsSlave);
    }


}