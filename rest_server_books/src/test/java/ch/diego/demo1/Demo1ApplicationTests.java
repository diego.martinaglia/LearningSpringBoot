package ch.diego.demo1;

import ch.diego.demo1.dbservice.DBService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@SpringBootTest
@ComponentScan({"ch.diego.demo1.dbservice","ch.diego.demo1.tasks"})
class Demo1ApplicationTests {

    @Test
    void contextLoads() {
    }

    @Component
    public static class SqlService {

        @Autowired
        @Qualifier("jdbcSlave")
        private JdbcTemplate jdbcTemplate;

        @Autowired
        DBService dbService;

        public String getHelloMessage() {
            String host = jdbcTemplate.queryForObject("select @@hostname;", String.class);
            System.out.println(host);
            return "Hello";
        }
    }
}
