package ch.diego.demo1.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.sql.SQLException;

@SpringBootApplication
@EnableCaching
@ComponentScan({"ch.diego.demo1.dbservice","ch.diego.demo1.web"})
public class StartRestAPI {

	public static void main(String[] args) {
		SpringApplication.run(StartRestAPI.class, args);
	}

}
