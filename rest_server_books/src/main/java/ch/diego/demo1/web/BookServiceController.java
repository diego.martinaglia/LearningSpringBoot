package ch.diego.demo1.web;

import ch.diego.demo1.dbservice.DBCloneService;
import ch.diego.demo1.dbservice.DBService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class BookServiceController {

    @Autowired
    DBService dbService;

    private static final Map<String, String> productRepo = new HashMap<>();

    @GetMapping(value = "/get_all_books")
    public ResponseEntity<Object> getAllBooks() {
        List<Map<String, Object>> result = dbService.getAllBooks();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/nbr_books_by_author")
    public ResponseEntity<Object> getAllBooksAuthor() {
        List<Map<String, Object>> result = dbService.getNbrBookyByAuthor();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/get_books_from_to/{fromId}/{toId}")
    public ResponseEntity<Object> getBooksFromTo(@PathVariable(value="fromId") String fromId,
                                              @PathVariable(value="toId") String toId) {
        List<Map<String, Object>> result = dbService.getBooksFromTo(fromId,toId);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @GetMapping(path = "/book_by_id/{id}")
    public ResponseEntity<Object> getBookyById(@PathVariable(value="id") String bookIDString) {
        Integer bookID = null;
        try {
            bookID = Integer.parseInt(bookIDString);
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        List<Map<String,Object>> result = dbService.getBookById(bookID);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/book_by_id2")
    public ResponseEntity<Object> getProductByParam(@RequestParam(value="id",required=false,defaultValue="1") String id) {
        System.out.println("----product requested Id=" + id);
        return new ResponseEntity<>(productRepo.get(id), HttpStatus.OK);
    }




    @GetMapping(path="/product/{id}")
    public ResponseEntity<Object> getProductByPathVariable(@PathVariable("id") String id) {
        System.out.println("----product by Path variable requested Id=" + id);
        return new ResponseEntity<>(productRepo.get(id), HttpStatus.OK);
    }

    @GetMapping(value = "/products2")
    public ResponseEntity<Object> getProduct2() {
        System.out.println("----product requested ------");
        HashMap<String,String> products = new HashMap<String,String>();
        products.put("id","23234");
        products.put("name","nice Product");
        return new ResponseEntity<>(products, HttpStatus.OK);
    }




}