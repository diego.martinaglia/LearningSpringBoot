package ch.diego.demo1.tasks;


import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ch.diego.demo1.dbservice.DBCloneService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ch.diego.demo1.dbservice.DBService;

@Component
public class ScheduledTasks {

    @Autowired
    DBCloneService dbCloneService;

    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(cron = "0 0 18 * * *")
    public void copyDataIntoH1() {
        log.info("Start loading DAta {}", dateFormat.format(new Date()));
        dbCloneService.copyFromDB1_to_DB2();
    }
}