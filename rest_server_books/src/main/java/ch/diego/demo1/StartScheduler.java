package ch.diego.demo1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan({"ch.diego.demo1.dbservice","ch.diego.demo1.tasks"})
public class StartScheduler {

    public static void main(String[] args) {
        SpringApplication.run(StartScheduler.class,args);
    }
}