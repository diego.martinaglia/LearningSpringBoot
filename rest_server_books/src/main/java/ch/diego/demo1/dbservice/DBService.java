package ch.diego.demo1.dbservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Service;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import java.nio.file.Files;

import java.io.IOException;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class DBService {


    @Autowired
    @Qualifier("jdbcSourceDB")
    NamedParameterJdbcTemplate jdbcTemplateSourceDB;

    @Autowired
    @Qualifier("jdbcDestinationDB")
    NamedParameterJdbcTemplate jdbcTemplateDestinationDB;

    @Cacheable(cacheNames = {"allBooks"})
    public List<Map<String, Object>> getAllBooks() {
        List<Map<String, Object>> result =  jdbcTemplateSourceDB.queryForList("SELECT * from BOOKS", new MapSqlParameterSource());
        return result;
    }


    @Cacheable(cacheNames = {"booksByFromTo"})
    public List<Map<String, Object>> getBooksFromTo(String fromId, String toId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("fromId",fromId).addValue("toID",toId);
        List<Map<String, Object>> result =  jdbcTemplateSourceDB.queryForList("SELECT * from BOOKS where book_id > :fromId and book_id < toId", parameters);
        return result;
    }



    @Cacheable(cacheNames = {"booksById"})
    public List<Map<String,Object>> getBookById(Integer id) {
        List<Map<String, Object>> result  =  jdbcTemplateSourceDB.queryForList("SELECT * FROM BOOKS WHERE book_id = :bookIdId ",
                new MapSqlParameterSource()
                        .addValue("bookIdId", id));
        return result;
    }

    @Cacheable(cacheNames = {"booksAuthor"})
    public List<Map<String,Object>>  getNbrBookyByAuthor()  {
        String sqlQuery= null;
        try {
            sqlQuery = readFile("./src/main/resources/sqlFiles/nbrBooksByAuthorAndMonth.sql");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        List<Map<String, Object>> result  =  jdbcTemplateSourceDB.queryForList(sqlQuery,new MapSqlParameterSource());
        return result;
    }

    private String readFile(final String filePathString) throws IOException {
        Path filePath = Path.of(filePathString);
        String content = Files.readString(filePath);
        return content;
    }






}
