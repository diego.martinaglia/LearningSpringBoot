package ch.diego.demo1.dbservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class DBCloneService {


    @Autowired
    @Qualifier("jdbcSourceDB")
    NamedParameterJdbcTemplate jdbcTemplateSourceDB;

    @Autowired
    @Qualifier("jdbcDestinationDB")
    NamedParameterJdbcTemplate jdbcTemplateDestinationDB;


    private String readFile(final String filePathString) throws IOException {
        Path filePath = Path.of(filePathString);
        String content = Files.readString(filePath);
        return content;
    }


    public List<Map<String,Object>> copyFromDB1_to_DB2() {
        String tableName = "TMP_BOOKS_AUTHOR";
        List<Map<String, Object>> results = null;
        String sqlQuery= null;
        try {
            sqlQuery = readFile("./src/main/resources/sqlFiles/booksByAuthor.sql");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            results    =  jdbcTemplateSourceDB.queryForList(sqlQuery,new MapSqlParameterSource());
        } catch (RuntimeException re) {
            return null;
        }

        if (results == null || results.size() == 0) {
            return null;
        }
        if (!tableExists(tableName) || (tableExists(tableName) && dropTable(tableName))) {
            for (Map<String, Object> result : results) {
                if (this.createTable(result,tableName)) {
                    break;
                }
            }
        }

        int counter=0;
        for (Map<String,Object> result  : results) {
            this.insertIntoTmpDB(result,tableName);
            counter ++;
            if (counter > results.size()) {
                System.err.println("----------------to much -.....");
            }
        }
        return results;
    }

    private boolean tableExists(String tableName)  {
        String sqlQueryTable = """
        SELECT table_name FROM information_schema.tables
        WHERE table_name = :tableName    LIMIT 1
        """;
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("tableName",tableName);
        try {
            List<Map<String, Object>> result = jdbcTemplateDestinationDB.queryForList(sqlQueryTable, parameters);
            if (result.size() > 0) {
                return true;
            }
        } catch (RuntimeException re) {
            System.out.println(("-----exception happend="+ re.toString()));
            return false;
        }
       return false;
    }

    private boolean dropTable(String tableName) {
        String DROP_TABLE_SQL = "DROP TABLE " + tableName  ;
        try {
            jdbcTemplateDestinationDB.getJdbcTemplate().execute(DROP_TABLE_SQL);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private boolean createTable(Map<String, Object> result, String tableName) {
        String CREATE_SQL = "CREATE TABLE " + tableName +  " (" ;

        for (Map.Entry<String, Object> entry : result.entrySet()) {
            Object value = entry.getValue();
            String dataType="";
            if (value == null) {
                return false;
            }
            if (value instanceof Integer) {
                dataType = "BIGINT";
            } else if(value instanceof String) {
                dataType = "VARCHAR(2056)";
            } else if(value instanceof Date) {
                dataType = "DATE";
            } else {
                throw new RuntimeException("-----------DataType not defined ---------");
            }
            CREATE_SQL = CREATE_SQL + "\n" +  entry.getKey() + " " + dataType + ",";
        }
        CREATE_SQL= CREATE_SQL + ")";
        CREATE_SQL= CREATE_SQL.replaceFirst(",\\)", ")\n");
        System.out.println("-----CREATE_SQL=" + CREATE_SQL);
        jdbcTemplateDestinationDB.getJdbcTemplate().execute(CREATE_SQL);
        return true;
    }

    private int insertIntoTmpDB (Map<String, Object> result, String tableName) {
        String INSERT_SQL = "INSERT INTO  ";
        INSERT_SQL = INSERT_SQL + tableName;
        String part1 = " (";
        String part2 = " values(";

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        for (Map.Entry<String, Object> entry : result.entrySet()) {
            part1 = part1 + entry.getKey() + ",";
            part2 = part2 + ":" + entry.getKey() + ",";
            parameters.addValue(entry.getKey(),entry.getValue());
        }
        part1=  part1.substring(0, part1.length() - 1) + ")";
        part2=  part2.substring(0, part2.length() - 1) + ")";

        INSERT_SQL = INSERT_SQL + part1 + part2 ;
        int nbr = jdbcTemplateDestinationDB.update(INSERT_SQL, parameters);


        return 0;

    }



}
