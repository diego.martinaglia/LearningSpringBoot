package ch.diego.demo1.dbservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfig2 {


    @Bean(name = "jdbcSourceDB")
    public NamedParameterJdbcTemplate db1DataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("org.h2.Driver");
        dataSourceBuilder.url("jdbc:h2:file:E:/data/bookdb");
        dataSourceBuilder.username("SA");
        dataSourceBuilder.password("");
        return new NamedParameterJdbcTemplate(dataSourceBuilder.build());
    }

    @Bean(name = "jdbcDestinationDB")
    public NamedParameterJdbcTemplate db2DataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("org.h2.Driver");
        dataSourceBuilder.url("jdbc:h2:file:E:/data/bookdb_copy");
        dataSourceBuilder.username("SA");
        dataSourceBuilder.password("");
        return new NamedParameterJdbcTemplate(dataSourceBuilder.build());
    }


}