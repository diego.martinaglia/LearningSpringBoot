package ch.diego.graphql_server_jpa_ex1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphqlServerJpaEx1Application {

	public static void main(String[] args) {
		SpringApplication.run(GraphqlServerJpaEx1Application.class, args);
	}

}
