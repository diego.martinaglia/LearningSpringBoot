package ch.diego.graphql_server_jpa_ex1.repository;

import ch.diego.graphql_server_jpa_ex1.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;




public interface AuthorRepository extends JpaRepository<Author, Long> {

}