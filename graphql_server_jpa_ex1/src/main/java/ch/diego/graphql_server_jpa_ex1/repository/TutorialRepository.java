package ch.diego.graphql_server_jpa_ex1.repository;


import ch.diego.graphql_server_jpa_ex1.model.Tutorial;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TutorialRepository extends JpaRepository<Tutorial, Long> {

}