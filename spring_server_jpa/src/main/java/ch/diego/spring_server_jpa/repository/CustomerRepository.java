package ch.diego.spring_server_jpa.repository;

import java.util.List;

import ch.diego.spring_server_jpa.model.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

    List<Customer> findByLastName(String lastName);

    Customer findById(long id);
}

