package ch.diego.spring_server_jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringServerJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringServerJpaApplication.class, args);
	}

}
